#include <SFML\System.hpp>
#include <SFML\Window.hpp>
#include <SFML\Graphics.hpp>
#include "QuadTree.h"

const sf::Vector2f SCREEN_SIZE(1440.0f, 900.0f);

struct Particle
{
  sf::Vector2f position;
};

int main()
{
  sf::RenderWindow window(sf::VideoMode((int)SCREEN_SIZE.x, (int)SCREEN_SIZE.y), "Quad Tree - C++ Simple Demo");
  window.setFramerateLimit(60);
  srand((unsigned)time(0));

  sf::Clock gameClock;
  float dt;

  sf::Font font;
  font.loadFromFile("arial.ttf");

  sf::Event event;

  QuadTree<Particle> particleTree(0, std::make_shared<Rectangle>(0.0f, 0.0f, SCREEN_SIZE.x, SCREEN_SIZE.y));

  bool pressed = false;

  while(window.isOpen())
  {
    window.clear(sf::Color(40, 40, 40));
    window.pollEvent(event);
    
    switch(event.type)
    {
    case sf::Event::Closed: window.close(); break;
    case sf::Event::MouseButtonPressed:
      if(!pressed)
      {
        pressed = true;
        particleTree.Insert(std::make_shared<Particle>(), std::make_shared<Rectangle>((float)event.mouseButton.x, (float)event.mouseButton.y, 5.0f, 5.0f));
      }
      break;
    case sf::Event::MouseButtonReleased:
      pressed = false;
      break;
    }

    sf::RectangleShape particle(sf::Vector2f(10.0f, 10.0f));

    particleTree.Draw(window, particle, font);
    window.display();
  }

  dt = gameClock.restart().asSeconds();

  return 0;
}