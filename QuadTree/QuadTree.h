#pragma once
#include <memory>
#include <vector>
#include <utility>
#include <SFML/Graphics.hpp>

struct Rectangle
{
  float left, top, width, height;

  Rectangle(float left, float top, float width, float height) : left{ left }, top{ top }, width{ width }, height{ height } { }
};

template<class T>
class QuadTree
{
private:
  std::shared_ptr<QuadTree<T>> m_children[4];

  static unsigned int m_maxCapacity;
  static unsigned int m_maxDepth;

  std::shared_ptr<Rectangle> m_bounds;
  unsigned int m_depth;

  std::vector<std::shared_ptr<T>> m_objects;
  std::vector<std::shared_ptr<Rectangle>> m_objectsAABB;

public:
  QuadTree(unsigned int depth, std::shared_ptr<Rectangle> bounds) : m_depth{ depth }, m_bounds{ bounds } { }
  ~QuadTree() { }

  void Insert(std::shared_ptr<T> toInsert, std::shared_ptr<Rectangle> bounds)
  {
    if(HasChildren())
    {
      int index = GetIndex(bounds); 

      // If we can, insert object into the child node and return
      if(index != -1)
      {
        m_children[index]->Insert(toInsert, bounds);
        return;
      }
    }

    // Else insert into this node and try to split
    m_objects.push_back(toInsert);
    m_objectsAABB.push_back(bounds);

    // If this node is full and isn't at maximum tree depth, split into 4 nodes
    if(m_objects.size() > m_maxCapacity && m_depth < m_maxDepth)
    {
      Split();

      for(int i = 0; i < m_objects.size(); ++i)
      {
        // Then, check if we can put any objects from the current node, into the newly made children
        int index = GetIndex(m_objectsAABB[i]);

        if(index != -1)
        {
          // Insert it into child node, then remove from this node
          m_children[index]->Insert(m_objects[i], m_objectsAABB[i]);

          std::swap(m_objects[i], m_objects.back());
          std::swap(m_objectsAABB[i], m_objectsAABB.back());

          m_objects.pop_back();
          m_objectsAABB.pop_back();
        }
      }
    }
  }

  void Clear()
  {
    m_objects.clear();
    m_objectsAABB.clear();

    for(int i = 0; i < 4; ++i)
    {
      if(m_children[i] == nullptr) continue;

      m_children[i]->Clear();
      m_children[i].reset();
    }
  }

  void Split()
  {
    float halfHeight = m_bounds->height / 2;
    float halfWidth = m_bounds->width / 2;

    // Only split if we don't have children to avoid deleting objects in children nodes when split is called on nodes less than m_maxDepth
    if(!HasChildren())
    {
      m_children[0] = std::make_shared<QuadTree<T>>(m_depth + 1, std::make_shared<Rectangle>(m_bounds->left, m_bounds->top, halfWidth, halfHeight));
      m_children[1] = std::make_shared<QuadTree<T>>(m_depth + 1, std::make_shared<Rectangle>(m_bounds->left + halfWidth, m_bounds->top, halfWidth, halfHeight));
      m_children[2] = std::make_shared<QuadTree<T>>(m_depth + 1, std::make_shared<Rectangle>(m_bounds->left, m_bounds->top + halfHeight, halfWidth, halfHeight));
      m_children[3] = std::make_shared<QuadTree<T>>(m_depth + 1, std::make_shared<Rectangle>(m_bounds->left + halfWidth, m_bounds->top + halfHeight, halfWidth, halfHeight));
    }
  }

  int GetIndex(std::shared_ptr<Rectangle> bounds)
  {
    // If it can't fit in any children, -1 will be returned meaning it only fits in parent node
    int index = -1;

    sf::Vector2f midPoint = sf::Vector2f(m_bounds->left + m_bounds->width / 2, m_bounds->top + m_bounds->height / 2);

    // Fits in one of the top nodes
    if(bounds->top + bounds->height < midPoint.y)
    {
      if(bounds->left + bounds->width < midPoint.x) index = 0; // Fits in top left
      else if(bounds->left > midPoint.x) index = 1; // Fits in top right
    }

    // Fits in one of the bottom nodes
    if(bounds->top > midPoint.y)
    {
      if(bounds->left + bounds->width < midPoint.x) index = 2; // Fits in bottom left
      else if(bounds->left > midPoint.x) index = 3; // Fits in bottom right
    }

    return index;
  }

  std::vector<T> RetrieveAllInNode(std::vector<T> toReturn, std::shared_ptr<Rectangle> bounds)
  {
    int index = GetIndex(bounds);

    if(index != -1 && HasChildren()) m_children[index]->RetrieveAllInNode(toReturn, bounds);

    toReturn.insert(toReturn.back(), m_objects);

    return toReturn;
  }

  bool HasChildren()
  {
    return m_children[0] != nullptr && m_children[1] != nullptr && m_children[2] != nullptr && m_children[3] != nullptr;
  }

  void Draw(sf::RenderWindow &window, sf::RectangleShape &toDraw, const sf::Font &font)
  {
    // Call Draw on every child
    for(auto &child : m_children) if(child != nullptr) child->Draw(window, toDraw, font);

#ifdef _DEBUG
    // Draw a rectangle showing this node's size
    sf::RectangleShape thisNode(sf::Vector2f(m_bounds->width, m_bounds->height));
    thisNode.setOutlineColor(sf::Color::White);
    thisNode.setOutlineThickness(1);
    thisNode.setFillColor(sf::Color::Transparent);
    thisNode.setPosition(m_bounds->left, m_bounds->top);
    window.draw(thisNode);

    // Draw amount of objects in current node
    sf::Text amount("#: " + std::to_string(m_objects.size()), font);
    amount.setColor(sf::Color::White);
    amount.setCharacterSize(16);
    amount.setPosition(m_bounds->left + m_bounds->width / 2, m_bounds->top + m_bounds->height / 2);
    window.draw(amount);
#endif

    // Once every child has drawn, draw this nodes objects
    for(int i = 0; i < m_objects.size(); ++i)
    {
      toDraw.setPosition(sf::Vector2f(m_objectsAABB[i]->left, m_objectsAABB[i]->top));
      window.draw(toDraw);
    }
  }

};

template<class T>
unsigned int QuadTree<T>::m_maxCapacity = 3;

template<class T>
unsigned int QuadTree<T>::m_maxDepth = 3;