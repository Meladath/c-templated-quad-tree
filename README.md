A simple templated quad tree made with C++.

Uses SFML 2.2 for window and event/input.

Templated and simple to use. Has some debug info drawing in the Draw function if built with debug mode.

Place sfml-graphics-2.dll, sfml-system-2.dll and sfml-window-2.dll (and their respective -d versions if running in debug mode) and arial.ttf in the build directory for running alone, or the project directory for running inside visual studio.